package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestVisualisationModel {

    private int id;
    private Integer senderId;
    private Integer receiverId;
    private String senderFirstName;
    private String senderLastName;
    private String senderPicture;
    private String senderLoginUsername;

}
