package com.gaming.project.gamingproject.models;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
public class LikeCommentModel {

    @PositiveOrZero
    @NotNull
    private int commentId;

    @PositiveOrZero
    @NotNull
    private int userId;
}
