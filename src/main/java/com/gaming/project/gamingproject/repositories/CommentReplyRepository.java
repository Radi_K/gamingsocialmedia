package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.CommentReply;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentReplyRepository extends CrudRepository<CommentReply, Integer> {
}
