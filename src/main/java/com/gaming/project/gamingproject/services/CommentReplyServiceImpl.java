package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.repositories.CommentReplyRepository;
import com.gaming.project.gamingproject.services.contracts.CommentReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentReplyServiceImpl implements CommentReplyService {

    private CommentReplyRepository commentReplyRepository;

    @Autowired
    public CommentReplyServiceImpl(CommentReplyRepository commentReplyRepository) {
        this.commentReplyRepository = commentReplyRepository;
    }
}
