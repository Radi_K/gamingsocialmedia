package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.CommentEditModel;
import com.gaming.project.gamingproject.models.CommentVisualizationModel;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.gaming.project.gamingproject.services.UserServiceImpl.copyNonNullProperties;

@Service
public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;
    private ModelMapper mapper;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, ModelMapper mapper) {
        this.commentRepository = commentRepository;
        this.mapper = mapper;
    }

    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Transactional
    @Override
    public void deleteCommentsByPostId(int postId) {
        commentRepository.deleteAllByPostId(postId);
    }

    @Override
    public Page<CommentVisualizationModel> findCommentsForUser(Pageable pageable, Integer userId) {
        return commentRepository.findAllByUserId(pageable, userId)
                .map(this::convertComment);
    }

    @Override
    public void deleteCommentById(Integer commentId) {
        commentRepository.delete(getById(commentId));
    }

    @Override
    public CommentEditModel editComment(CommentEditModel commentModel) {
        Comment comment = getById(commentModel.getId());
        copyNonNullProperties(convertEditModelToEntity(commentModel), comment);
        return convertCommentEditModel(commentRepository.save(comment));
    }

    @Override
    public Comment createComment(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public CommentVisualizationModel convertComment(Comment comment) {
        return mapper.map(comment, CommentVisualizationModel.class);
    }

    @Override
    public Comment getById(int id) {
        return commentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Comment does not exist."));
    }

    private CommentEditModel convertCommentEditModel(Comment comment) {
        return mapper.map(comment, CommentEditModel.class);
    }

    private Comment convertEditModelToEntity(CommentEditModel commentModel) {
        return mapper.map(commentModel, Comment.class);
    }
}
