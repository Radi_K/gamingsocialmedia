package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Like;
import com.gaming.project.gamingproject.models.LikeModel;

import java.security.Principal;

public interface LikeService {

    boolean existsByPostIdAndUserId(int postId, int userId);

    void createLike(Like like);

    Like getByUserIdAndPostId(int userId, int beerId);

    Integer addLikeToPost(LikeModel likeModel, Principal principal);

    Integer dislike(LikeModel likeModel, Principal principal);
}
