package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.*;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.*;
import com.gaming.project.gamingproject.repositories.PostRepository;
import com.gaming.project.gamingproject.services.contracts.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

import static com.gaming.project.gamingproject.services.UserServiceImpl.copyNonNullProperties;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;
    private UserService userService;
    private CommentService commentService;
    private ModelMapper mapper;
    private FriendService friendService;
    private LikeCommentService likeCommentService;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserService userService, CommentService commentService,
                           ModelMapper mapper, FriendService friendService, LikeCommentService likeCommentService) {
        this.postRepository = postRepository;
        this.userService = userService;
        this.commentService = commentService;
        this.mapper = mapper;
        this.friendService = friendService;
        this.likeCommentService = likeCommentService;
    }

    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public Post createPost(Post post) {
        return postRepository.save(post);
    }

    @Override
    public Post getById(int id) {
        return postRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Post does not exist."));
    }

    @Transactional
    @Override
    public void deletePost(Integer postId) {
        if (!postRepository.existsById(postId)) {
            throw new EntityNotFoundException("Post Not Found");
        }
        Post post = getById(postId);
        if (!post.getComments().isEmpty()) {
            post.getComments()
                    .forEach(el -> likeCommentService.deleteCommentLikesByCommentId(el.getId()));
        }
        commentService.deleteCommentsByPostId(postId);
        postRepository.delete(post);
    }

    @Override
    public PostEditModel editPost(PostEditModel postModel) {
        Post post = postRepository.findById(postModel.getId()).orElseThrow(EntityNotFoundException::new);
        copyNonNullProperties(convertEditModelToEntity(postModel), post);
        userService.addUser(userService.findById(post.getUser().getId()));
        return convertPostEditModel(postRepository.save(post));
    }

    @Override
    public Page<PostVisualisationModel> getPostForUserPage(Pageable pageable, Principal principal) {
        User user = userService.getUser(principal.getName());

        return postRepository.findByUserOrderByUpdateTimeDesc(pageable, user)
                .map(el -> convertPost(el, user));
    }

    @Override
    public Page<PostVisualisationModel> findTop5PostsByLikes(Pageable pageable, Principal principal) {
        User user = userService.getUser(principal.getName());
        return postRepository.findAllTopFiveForUser(pageable, user.getId())
                .map(el -> convertPost(el, user));
    }

    @Transactional
    @Override
    public CommentVisualizationModel addCommentToPost(CommentModel commentModel, Principal principal) {
        User user = userService.getUser(principal.getName());
        Post post = getById(commentModel.getPostId());
        post.setUpdateTime(LocalDateTime.now());

        Comment comment = new Comment(commentModel.getText(), 0, post, user, LocalDateTime.now());

        createPost(post);
        return commentService.convertComment(commentService.createComment(comment));
    }

    @Override
    public Page<PostVisualisationModel> findAllPublicPosts(Pageable pageable) {
        return postRepository.findAllPublicPosts(pageable).map(this::convertPost);
    }

    @Override
    public Page<PostVisualisationModel> findPostsForUser(Pageable pageable, Integer userId) {
        User user = userService.findById(userId);
        return postRepository.findAllByUser(pageable, user)
                .map(this::convertPost);
    }

    @Override
    public Page<PostVisualisationModel> findPostsByUsername(Pageable pageable, String userLoginUsername, Principal principal) {
        User user = userService.getUser(userLoginUsername);
        User loggedUser = userService.getUser(principal.getName());
        Page<PostVisualisationModel> posts = null;
        if (friendService.doesFriendshipExist(user, loggedUser) || friendService.doesFriendshipExist(loggedUser, user)) {
            posts = postRepository
                    .findAllByUserLoginUsernameOrderByIdDesc(userLoginUsername, pageable)
                    .map(el -> convertPost(el, loggedUser));
        } else {
            posts = postRepository
                    .findByUserLoginUsernameAndPostAccessibilityEqualsOrderByIdDesc(
                            userLoginUsername, pageable, PostAccessibility.PUBLIC)
                    .map(el -> convertPost(el, loggedUser));
        }

        if (posts.isEmpty()) {
            throw new EntityNotFoundException("No existing posts");
        }
        return posts;
    }

    @Override
    public Page<PostVisualisationModel> postsPagination(Pageable pageable, Principal principal) {
        User user = userService.getUser(principal.getName());
        return postRepository.findAllByUserId(pageable, user.getId())
                .map(el -> convertPost(el, user));
    }

    @Transactional
    @Override
    public PostVisualisationModel addPost(PostModel postModel, Principal principal) {
        User user = userService.getUser(principal.getName());
            Post post = Post.builder()
                .text(postModel.getText())
                .user(user)
                .postAccessibility(PostAccessibility.valueOf(postModel.getPostAccessibility()))
                .creationTime(LocalDateTime.now())
                .updateTime(LocalDateTime.now())
                .videoId(postModel.getVideoId())
                    .photo(postModel.getPhoto())
                .build();

        user.getPosts().add(post);
        return convertPost(createPost(post));
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setLikeCommentService(LikeCommentService likeCommentService) {
        this.likeCommentService = likeCommentService;
    }

    private PostVisualisationModel convertPost(Post post) {
        return mapper.map(post, PostVisualisationModel.class);
    }

    private PostEditModel convertPostEditModel(Post post) {
        return mapper.map(post, PostEditModel.class);
    }

    private PostVisualisationModel convertPost(Post post, User user) {
        PostVisualisationModel postVisualisationModel = mapper.map(post, PostVisualisationModel.class);
        post.getLikes().stream()
                .filter(el -> el.getUser().getId() == user.getId())
                .map(el -> true)
                .forEach(postVisualisationModel::setLiked);

        for (CommentVisualizationModel el : postVisualisationModel.getComments()) {
            for (LikeCommentModel com: el.getLikes()) {
                if (com.getUserId() == user.getId()) {
                    el.setLiked(true);
                }
            }
        }
        return postVisualisationModel;
    }

    private Post convertEditModelToEntity(PostEditModel editModel) {
        return mapper.map(editModel, Post.class);
    }
}
