package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.entities.UserCountries;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.PostService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/user{userName}")
    public String showPersonProfilePage(@PathVariable String userName, Model model, Authentication principal) {

        User user = userService.getUser(userName);
        User logged =  userService.getUser(principal.getName());

        model.addAttribute("user", user);
        model.addAttribute("logged", logged);
        model.addAttribute("userRole", userService.getUserRole(principal));

        if (principal.getName().equals(userName)) {
            return "redirect:/user";
        } else {
            return "user-page-other";
        }
    }

    @GetMapping("/user")
    public String getAllPostsPage(Model model, Authentication principal) {
        User user = userService.getUser(principal.getName());

        model.addAttribute("user", user);
        model.addAttribute("countries", UserCountries.values());
        model.addAttribute("genders", GenderType.values());
        model.addAttribute("userRole", userService.getUserRole(principal));

        return "user-page";
    }
}
