package com.gaming.project.gamingproject.entities;

import javafx.geometry.Pos;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "text")
    @NotNull
    private String text;

    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "post_id")
    private List<Like> likes;

    @Column(name = "likes")
    private int totalLikes;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany
    @JoinColumn(name = "post_id")
    private List<Comment> comments;

    @Enumerated(EnumType.STRING)
    private PostAccessibility postAccessibility;

    @Column(name = "creation_time")
    @Convert(converter = DataTimeConverter.class)
    private LocalDateTime creationTime;

    @Column(name = "updated_time")
    @Convert(converter = DataTimeConverter.class)
    private LocalDateTime updateTime;

    @Column(name = "video_id")
    private String videoId;

    @Column(name = "photo")
    private String photo;

    public String getCreationTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        return creationTime.format(formatter);
    }
}
