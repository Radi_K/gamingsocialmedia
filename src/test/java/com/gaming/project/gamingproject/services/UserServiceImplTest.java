package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.repositories.*;
import com.gaming.project.gamingproject.services.contracts.AuthoritiesService;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import com.gaming.project.gamingproject.services.contracts.LikeCommentService;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    private User testUser = new User();
    private User testUser2 = new User();
    private User loggedUser = new User();

    @Mock
    LikeCommentService likeCommentService;
    @Mock
    LikeCommentRepository likeCommentRepository;
    @Mock
    AuthoritiesService authoritiesService;
    @Mock
    AuthoritiesRepository authoritiesRepository;
    @Mock
    RequestService requestService;
    @Mock
    FriendService friendService;
    @Mock
    Principal principal;
    @Mock
    UserRepository userRepository;
    @Mock
    LoginRepository loginRepository;
    @Mock
    RequestRepository requestRepository;
    @InjectMocks
    UserServiceImpl userService;
    @Mock
    ModelMapper modelMapper;


    @Before
    public void setUp() {

        testUser.setId(1);
        testUser2.setId(2);
        testUser.setLogin(new Login("ivan", "123321", true));
    }

    @Test
    public void addUser_Should_addUserSuccessfully() {
        userService.addUser(new User());

        Mockito.verify(userRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void findById_Should_ReturnRightUser_WhenExist() {
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(new User()));
        User userResult = userService.findById(1);


        assertEquals(0, userResult.getId());
    }

    @Test
    public void findByIdModel_Should_ReturnRightUserModel_WhenExist() {
        UserModel userModel = new UserModel();
        userModel.setId(1);
        User user = new User();
        user.setId(1);

        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(user));
        Mockito.when(userService.findByIdModel(1)).thenReturn(userModel);

        UserModel userResult = userService.findByIdModel(1);

        assertEquals(Optional.of(1), Optional.ofNullable(userResult.getId()));
    }

    @Test
    public void countUsers_Should_ReturnRight_amount() {

        Mockito.when(userRepository.count()).thenReturn(Long.valueOf(2));

        assertEquals(Long.valueOf(2), userService.countUsers());
    }

    @Test
    public void createUser_Should_ReturnNewUserAdded() {
        userService.setLoginService(new LoginServiceImpl(loginRepository));

        Mockito.when(loginRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(testUser.getLogin()));

        RegisterUserModel registerUserModel = new RegisterUserModel();
        registerUserModel.setFirstName("Ivan");
        registerUserModel.setLastName("Ivanov");
        registerUserModel.setEmail("Ivan@abv.bg");
        registerUserModel.setAge(15);
        registerUserModel.setGender(GenderType.MALE);
        registerUserModel.setCountry("Bulgaria");
        registerUserModel.setPicture("avatar3.png");

        userService.createUser(registerUserModel);

        Mockito.verify(userRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void sendRequest_Should_ReturnNewUserAdded() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(testUser));
        Mockito.when(userRepository.findFirstByLoginUsername(Mockito.any())).thenReturn(loggedUser);
        Mockito.when(requestService.doesRequestExist(Mockito.any(), Mockito.any())).thenReturn(false);

        userService.sendRequest(1, principal);

        Mockito.verify(requestService, Mockito.times(1)).createRequest(Mockito.any());
    }

//    @Test
//    public void AddAsFriend_Should_AcceptTheRequest() {
//        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(testUser));
//        Mockito.when(userRepository.findFirstByLoginUsername(Mockito.any())).thenReturn(loggedUser);
//        Mockito.when(requestService.doesRequestExist(Mockito.any(), Mockito.any())).thenReturn(true);
//
//        userService.addAsFriend(1, principal);
//
//        Mockito.verify(friendService, Mockito.times(1)).createFriendship(Mockito.any());
//    }

    @Test(expected = EntityNotFoundException.class)
    public void addAsFriend_Should_throw_Exception_when_entityDoesNotExist() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(testUser));
        Mockito.when(userRepository.findFirstByLoginUsername(Mockito.any())).thenReturn(loggedUser);
        Mockito.when(requestService.doesRequestExist(Mockito.any(), Mockito.any())).thenReturn(false);

        userService.addAsFriend(1, principal);

        Mockito.verify(friendService, Mockito.times(1)).createFriendship(Mockito.any());
    }

    @Test
    public void removeAsFriend_Should_RemoveFriend() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(testUser));
        Mockito.when(userRepository.findFirstByLoginUsername(Mockito.any())).thenReturn(loggedUser);
        Mockito.when(friendService.doesFriendshipExist(Mockito.any(), Mockito.any())).thenReturn(true);

        userService.removeFromFriends(1, principal);

        Mockito.verify(friendService, Mockito.times(1)).deleteFriendShip(Mockito.any(), Mockito.any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeAsFriend_Should_throw_Exception_when_entityDoesNotExist() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(testUser));
        Mockito.when(userRepository.findFirstByLoginUsername(Mockito.any())).thenReturn(loggedUser);
        Mockito.when(friendService.doesFriendshipExist(Mockito.any(), Mockito.any())).thenReturn(false);

        userService.removeFromFriends(1, principal);

        Mockito.verify(friendService, Mockito.times(1)).deleteFriendShip(Mockito.any(), Mockito.any());
    }

    @Test
    public void removeFriendRequest_Should_remove_request() {
        userService.setLoginService(new LoginServiceImpl(loginRepository));
        userService.setRequestService(new RequestServiceImpl(requestRepository));

        Mockito.when(userRepository.findById(1)).thenReturn(Optional.ofNullable(testUser));
        Mockito.when(userRepository.findById(2)).thenReturn(Optional.ofNullable(testUser2));
        Mockito.when(requestRepository.existsBySenderIdAndReceiverId(1, 2)).thenReturn(true);

        userService.removeFriendRequest(1, 2);

        Mockito.verify(requestRepository, Mockito.times(1)).deleteBySenderAndReceiver(Mockito.any(), Mockito.any());
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeFriendRequest_Should_throw_Exception_when_entityDoesNotExist() {
        userService.setLoginService(new LoginServiceImpl(loginRepository));
        userService.setRequestService(new RequestServiceImpl(requestRepository));

        Mockito.when(userRepository.findById(1)).thenReturn(Optional.ofNullable(testUser));
        Mockito.when(userRepository.findById(2)).thenReturn(Optional.ofNullable(testUser2));

        Mockito.when(requestRepository.existsBySenderIdAndReceiverId(2, 1)).thenReturn(false);

        userService.removeFriendRequest(1, 2);

        Mockito.verify(requestRepository, Mockito.times(1)).deleteBySenderAndReceiver(Mockito.any(), Mockito.any());
    }

    @Test
    public void findAllByGenderType_Should_return_listOfUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());

        Mockito.when(userRepository.findByGenderEquals(GenderType.MALE)).thenReturn(users);

        assertEquals(2, userService.findAllByGenderType(GenderType.MALE).size());
    }

    @Test
    public void findUserById_should_return_UserRequestCheckModel() {
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.ofNullable(testUser));

        userService.findUserById(1, principal);

        Mockito.verify(userRepository, Mockito.times(1)).findById(Mockito.any());
    }

//    @Test
//    public void deleteUserById_Should_remove_user() {
//        userService.setLikeCommentService(new LikeCommentServiceImpl(likeCommentRepository));
//        userService.setAuthoritiesService(new AuthoritiesServiceImpl(authoritiesRepository));
//
//        Mockito.when(userRepository.existsById(1)).thenReturn(true);
//        Mockito.when(userRepository.findById(1)).thenReturn(Optional.ofNullable(testUser));
//
//        userService.deleteUserById(1);
//
//        Mockito.verify(userRepository, Mockito.times(1)).delete(testUser);
//    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteUserById_Should_throw_exception_whenUserNotFound() {
        userService.setLikeCommentService(new LikeCommentServiceImpl(likeCommentRepository));
        userService.setAuthoritiesService(new AuthoritiesServiceImpl(authoritiesRepository));

        userService.deleteUserById(1);

        Mockito.verify(userRepository, Mockito.times(1)).delete(testUser);
    }
}
