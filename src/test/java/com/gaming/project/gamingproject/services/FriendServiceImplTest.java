package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.repositories.FriendRepository;
import com.gaming.project.gamingproject.repositories.LoginRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FriendServiceImplTest {

    private User testSender = new User();
    private User testReceiver = new User();

    @InjectMocks
    FriendServiceImpl friendService;
    @Mock
    FriendRepository friendRepository;

    @Before
    public void setUp() {
        testSender.setId(1);
        testReceiver.setId(1);
    }

    @Test
    public void findByUsername_should_return_correctLogin() {
        friendService.createFriendship(new Friend());

        Mockito.verify(friendRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void doesFriendshipExist_should_check_ifExist() {

        Mockito.when(friendService.doesFriendshipExist(testSender, testReceiver)).thenReturn(true);

        friendService.doesFriendshipExist(testSender, testReceiver);

        Mockito.verify(friendRepository, Mockito.times(1)).existsBySenderIdAndReceiverId(1, 1);
    }

    @Test
    public void deleteFriendShip_should_deleteFriendship() {

        friendService.deleteFriendShip(testSender, testReceiver);

        Mockito.verify(friendRepository, Mockito.times(1)).deleteBySenderAndReceiver(Mockito.any(), Mockito.any());
    }

    @Test
    public void findFriendsByUserName_Should_return_listOfFriends() {
        List<Friend> friends = new ArrayList<>();
        friends.add(new Friend());
        friends.add(new Friend());

        Mockito.when(friendRepository.findAllByReceiver_Login_Username(Mockito.any())).thenReturn(friends);

        assertEquals(2, friendService.findFriendsByUserName(Mockito.any()).size());
    }
}
