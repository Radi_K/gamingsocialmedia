package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.models.LoginModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.repositories.LoginRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceImplTest {

    private LoginModel loginModel = new LoginModel();
    private RegisterUserModel registerUserModel = new RegisterUserModel();

    @InjectMocks
    LoginServiceImpl loginService;
    @Mock
    LoginRepository loginRepository;
    @Mock
    UserDetailsManager userDetailsManager;

    @Before
    public void setUp() {
//        registerUserModel.setLogin(new Login("Ivan","123", true));
//        registerUserModel.setPicture("picture7");
//        registerUserModel.setCountry("Bulgaria");
//        registerUserModel.setGender(GenderType.MALE);
//        registerUserModel.setAge(22);
//        registerUserModel.setEmail("asd@abv.bg");
//        registerUserModel.setLastName("Ivanov");
//        registerUserModel.setFirstName("Ivan");
//        registerUserModel.setDescription("asd");
//        registerUserModel.setLoginPassword("123");
//        registerUserModel.setPasswordConfirmation("123");
//
//        loginModel.setId(1);
//        loginModel.setPassword("asd");
//        loginModel.setPasswordConfirmation("asd");
//        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
//        org.springframework.security.core.userdetails.User newUser =
//                new org.springframework.security.core.userdetails.User(
//                        loginModel.getUsername(), loginModel.getPassword(), false,
//                        true, true, true, authorities);
    }
    @Test
    public void findByUsername_should_return_correctLogin() {
        Mockito.when(loginRepository.findById(Mockito.any())).thenReturn(java.util.Optional.of(new Login()));

        loginService.findByUsername("asd");

        Mockito.verify(loginRepository, Mockito.times(1)).findById(Mockito.any());
    }

//    @Test
//    public void createLogin_should_createLogin() {
//        loginService.createLogin(new RegisterUserModel());
//
//
//    }
}
