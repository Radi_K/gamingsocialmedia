package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Like;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.models.LikeModel;
import com.gaming.project.gamingproject.repositories.LikeRepository;
import com.gaming.project.gamingproject.repositories.PostRepository;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.PostService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LikeServiceImplTest {

    private Like testLike = new Like();
    private User testUser = new User();
    private Post testPost = new Post();
    LikeModel likeModel = new LikeModel();
    private LikeCommentModel likeCommentModel = new LikeCommentModel();

    @InjectMocks
    LikeServiceImpl likeService;
    @Mock
    LikeRepository likeRepository;
    @Mock
    PostService postService;
    @Mock
    PostRepository postRepository;
    @Mock
    UserServiceImpl userService;
    @Mock
    UserRepository userRepository;
    @Mock
    Principal principal;



    @Before
    public void setUp() {
        likeCommentModel.setCommentId(1);
        likeCommentModel.setUserId(1);

        testLike.setId(1);

        testUser.setId(1);
        testUser.setLogin(new Login("ivan", "123321", true));
        testUser.setFirstName("Ivan");

        testPost.setId(1);

        likeModel.setPostId(1);
    }

    @Test
    public void existsByPostIdAndUserId_Should_returnInformationAboutExisting() {
        when(likeService.existsByPostIdAndUserId(1,1)).thenReturn(true);

        likeService.existsByPostIdAndUserId(1,1);

        Mockito.verify(likeRepository, Mockito.times(1)).existsByPostIdAndUserId(1,1);
    }

    @Test
    public void addUser_Should_addUserSuccessfully() {
        likeService.createLike(new Like());

        Mockito.verify(likeRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void getLike_by_PostIdAndUserId_should_return_Like() {
        when(likeRepository.findByUserIdAndPostId(1,1)).thenReturn(testLike);
        when(likeService.getByUserIdAndPostId(1,1)).thenReturn(testLike);

        likeService.getByUserIdAndPostId(1,1);

        Mockito.verify(likeRepository, Mockito.times(1)).findByUserIdAndPostId(1, 1);
    }

    @Test
    public void addLikeToPost_should_add_Like() {
        likeService.setPostService(new PostServiceImpl(postRepository));

        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.of(testPost));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);

        likeService.addLikeToPost(likeModel,principal);

        Mockito.verify(postRepository, Mockito.times(1)).save(testPost);
    }

    @Test(expected = EntityNotFoundException.class)
    public void addLikeToPost_should_throw_exception_whenLikeDoesNotExist() {
        likeService.setPostService(new PostServiceImpl(postRepository));

        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.of(testPost));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);
        Mockito.when(likeService.existsByPostIdAndUserId(1,1)).thenReturn(true);

        likeService.addLikeToPost(likeModel,principal);

        Mockito.verify(postRepository, Mockito.times(1)).save(testPost);
    }

    @Test
    public void addDislikeToPost_should_remove_like() {
        likeService.setPostService(new PostServiceImpl(postRepository));

        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.of(testPost));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);
        Mockito.when(likeService.getByUserIdAndPostId(1,1)).thenReturn(testLike);
        Mockito.when(likeService.existsByPostIdAndUserId(1,1)).thenReturn(true);

        likeService.dislike(likeModel,principal);

        Mockito.verify(postRepository, Mockito.times(1)).save(testPost);

    }

    @Test(expected = EntityNotFoundException.class)
    public void addDislikeToPost_should_throw_exception_whenLikeDoesNotExist() {
        likeService.setPostService(new PostServiceImpl(postRepository));

        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.of(testPost));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);
        Mockito.when(likeService.getByUserIdAndPostId(1,1)).thenReturn(testLike);
        Mockito.when(likeService.existsByPostIdAndUserId(1,1)).thenReturn(false);

        likeService.dislike(likeModel,principal);

        Mockito.verify(postRepository, Mockito.times(1)).save(testPost);
    }
}
